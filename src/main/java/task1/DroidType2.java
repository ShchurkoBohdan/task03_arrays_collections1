package task1;

public class DroidType2 extends Droid{
    int health;

    public DroidType2(String name, int health) {
        super(name);
        this.health = health;
    }

    public void run(){
        System.out.println("Droid 2 run.");
    }

    public void fight(){
        System.out.println("Droid 2 fight.");
    }
}
