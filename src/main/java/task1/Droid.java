package task1;

public class Droid implements DroidInterface{
    private String name;

    public Droid(String name) {
        this.name = name;
    }

    public void run(){}

    public void fight(){}

    public void defend(){}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
