package task1;

public class DroidType1 extends Droid{
    int health;

    public DroidType1(String name, int health) {
        super(name);
        this.health = health;
    }

    public void run(){
        System.out.println("Droid 1 run.");
    }

    public void fight(){
        System.out.println("Droid 1 fight.");
    }
}
